/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	  https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package clauger.com.uploadingfiles.storage;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class FileSystemStorageServiceTests {

	private StorageProperties properties = new StorageProperties();
	private FileSystemStorageService service;


	@BeforeEach
	public void init() {
		properties.setLocation("target/files/" + Math.abs(new Random().nextLong()));
		service = new FileSystemStorageService(properties);
		service.init();

	}

	@Test
	public void loadNonExistent() {

		assertThat(service.load("doesNotExisit.txt")).doesNotExist();
	}

	@Test
	public void saveAndLoad() {
		service.store(new MockMultipartFile("first", "first.txt", MediaType.TEXT_PLAIN_VALUE,
				"Hello, World".getBytes()));
		assertThat(service.load("first.txt")).exists();
	}

	@Test
	public void saveRelativePathNotPermitted() {
		assertThrows(StorageException.class, () -> {
			service.store(new MockMultipartFile("foo", "../foo.txt",
					MediaType.TEXT_PLAIN_VALUE, "Hello, World".getBytes()));
		});
	}

	@Test
	public void saveAbsolutePathNotPermitted() {
		assertThrows(StorageException.class, () -> {
			service.store(new MockMultipartFile("foo", "/etc/passwd",
					MediaType.TEXT_PLAIN_VALUE, "Hello, World".getBytes()));
		});
	}

	@Test
	@EnabledOnOs({OS.LINUX})
	public void saveAbsolutePathInFilenamePermitted() {
		//Unix file systems (e.g. ext4) allows backslash '\' in file names.
		String fileName="\\etc\\passwd";
		service.store(new MockMultipartFile(fileName, fileName,
				MediaType.TEXT_PLAIN_VALUE, "Hello, World".getBytes()));
		assertTrue(Files.exists(
				Paths.get(properties.getLocation()).resolve(Paths.get(fileName))));
	}

	@Test
	public void savePermitted() {
		service.store(new MockMultipartFile("foo", "bar/../foo.csv",
				"text/csv", "Hello, World".getBytes()));
		assertThat(service.load("foo.csv")).exists();
	}
	@Test
	public void saveExtensionNotPermitted() {
		MockMultipartFile file = new MockMultipartFile("first", "first.txt", MediaType.TEXT_PLAIN_VALUE,
				"Hello, World".getBytes());
		if((file.getContentType().equals("text/csv"))){
			service.store(file);
		}
		assertThat(service.load("first.txt")).doesNotExist();
	}
	@Test
	public void shouldDeleteAll() {
		service.store(new MockMultipartFile("first", "first.txt", MediaType.TEXT_PLAIN_VALUE,
				"Hello, World".getBytes()));
		assertThat(service.load("first.txt")).exists();;
		service.store(new MockMultipartFile("second", "second.txt", MediaType.TEXT_PLAIN_VALUE,
				"Hello, World".getBytes()));
		assertThat(service.load("second.txt")).exists();;
		service.deleteAll();
		service.init();
		assertThat(service.load("second.txt")).doesNotExist();
		assertThat(service.load("first.txt")).doesNotExist();

	}

}
