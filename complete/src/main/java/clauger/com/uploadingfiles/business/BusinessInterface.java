package clauger.com.uploadingfiles.business;

import clauger.com.uploadingfiles.model.FileUploadDTO;
import clauger.com.uploadingfiles.model.FileUploaded;
import org.springframework.core.io.Resource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Stream;

public interface BusinessInterface {

    String deleteAll();

    FileUploaded loadFile(String originalFilename) throws IOException;

    public HashMap<String, ArrayList<FileUploaded> > uploadFilesBusiness(MultipartFile[] files);
    public ArrayList<FileUploadDTO> filesToDto(ArrayList<FileUploaded> filesToTransfer);

}
