package clauger.com.uploadingfiles.business;

import clauger.com.uploadingfiles.model.FileUploadDTO;
import clauger.com.uploadingfiles.model.FileUploaded;
import clauger.com.uploadingfiles.model.LineReport;
import clauger.com.uploadingfiles.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

@Service
public class ImplementationBusinessInterface implements BusinessInterface{

    private final StorageService storageService;

    @Autowired
    public ImplementationBusinessInterface(StorageService storageService) {

        this.storageService = storageService;
    }

    @Override
    public String deleteAll() {
        storageService.deleteAll();
        storageService.init();

        return   "All files were deleted Successfully";
    }

    @Override
    public FileUploaded loadFile(String originalFilename) throws IOException {
        return null;
    }

    @Override
    public HashMap<String,ArrayList<FileUploaded> > uploadFilesBusiness(MultipartFile[] files) {
        String message="";
        ArrayList<FileUploaded> filesResult= new ArrayList<>();
        HashMap<String,ArrayList<FileUploaded> > resultFinal= new HashMap<>();
        //Clear all old files
        storageService.deleteAll();
        storageService.init();
        try {
            List<String> fileNames = new ArrayList<>();
            int fileSize= files.length;
           // System.out.println("File size: "+fileSize);

            if(fileSize==2) {
                AtomicBoolean invalidFile= new AtomicBoolean(false);
                //Verify extension
                Arrays.asList(files).stream().forEach(file -> {
                  //  System.out.println("File: \n"+file.getContentType()+ "\n"+file.getOriginalFilename()+"\n"+file.getSize());
               if((file.getSize()==0)||!(file.getContentType().equals("text/csv"))){
                   invalidFile.set(true);
               }
                });
                if(invalidFile.get()) {
                    message= "You have load a empty file or invalid extension";
                    resultFinal.put(message,filesResult);

                }
                else{
                    Arrays.asList(files).stream().forEach(file -> {
                        storageService.store(file);
                        fileNames.add(file.getOriginalFilename());
                    });

                    message = "Uploaded the files successfully: " + fileNames.get(0) + " and " + fileNames.get(1);


                    ArrayList<FileUploaded> filesToCompare = new ArrayList<>();
                    Arrays.asList(files).stream().forEach(file -> {
                        try {
                            filesToCompare.add(storageService.loadFile(file.getOriginalFilename()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });


                    //filesToCompare.stream().forEach(System.out::println);

                    resultFinal.put(message, CompareFiles(filesToCompare));
                }

                return resultFinal;
            }
            else if(fileSize>2){
                message= "You need to load only two CSV files";
                resultFinal.put(message,filesResult);

                return resultFinal;
            }
            else{
                message= "You need to load at least two CSV files";
                resultFinal.put(message,filesResult);
                return resultFinal;

            }
        } catch (Exception e) {
            message = "Fail to upload files!"+" "+ HttpStatus.EXPECTATION_FAILED;
            resultFinal.put(message,filesResult);
            return resultFinal;
        }
    }

    @Override
    public ArrayList<FileUploadDTO> filesToDto(ArrayList<FileUploaded> filesToTransfer) {
        ArrayList<FileUploadDTO> fileDTOReturn= new ArrayList<>();


        for (FileUploaded file:filesToTransfer
        ) {
            FileUploadDTO fileDTO = new FileUploadDTO();
            fileDTO.setName(file.getName());
            fileDTO.setTotalRecord(file.getTotalRecord());
            fileDTO.setMatchingRecord(file.getMatchingRecord());
            fileDTO.setUnMatchingRecord(file.getUnMatchingRecord());
            ArrayList<LineReport> linesToDTO= new ArrayList<>();
            //Load Lines unmatched
            //file.getLineUnmatched().forEach(System.out::println);
            for (String linesToLoad:file.getLineUnmatched()
            ) {
                LineReport lineStract = new LineReport();
                String stract[]= linesToLoad.split(",");
                if(stract.length>6) {
                    if(stract.length>7) {
                        lineStract.setTransactionDate(stract[1]);
                        lineStract.setTransactionAmount(stract[2]);
                        lineStract.setTransactionNarrative(stract[3]);
                        lineStract.setTransactionDescription(stract[4]);
                        lineStract.setTransactionID(stract[5]);
                        lineStract.setTransactionType(stract[6]);
                        lineStract.setWalletReference(stract[7]);
                    }
                    else{
                        lineStract.setTransactionDate(stract[1]);
                        lineStract.setTransactionAmount(stract[2]);
                        lineStract.setTransactionNarrative(stract[3]);
                        lineStract.setTransactionDescription(stract[4]);
                        lineStract.setTransactionID(stract[5]);
                        lineStract.setTransactionType(stract[6]);

                    }
                }
                linesToDTO.add(lineStract);
            }
            fileDTO.setLineUnmatched(linesToDTO);
            fileDTOReturn.add(fileDTO);

        }
        return fileDTOReturn;
    }

    private ArrayList<FileUploaded> CompareFiles(ArrayList<FileUploaded> filesToCompare) {



        HashMap<String, String> linesFileOne = filesToCompare.get(0).getLines();
        HashMap<String, String> linesFileTwo = filesToCompare.get(1).getLines();
        ArrayList<String> linesToAdd = new ArrayList<>();
        ArrayList<String> linesToAdd2 = new ArrayList<>();

        for (String name: linesFileOne.keySet()) {
			String key = name.toString();
			String value= linesFileOne.get(name).toString();

//			System.out.println("\nkey:"+key + "\nvalue:" + value);
			if(!linesFileTwo.containsValue(value)){
			    linesToAdd.add(key);
			   // System.out.println("File one don't have value: "+value);
            }
            else if(!linesFileTwo.containsKey(key)){
              //  System.out.println("File one don't have key: "+key);
                linesToAdd.add(key);
            }
		}



        filesToCompare.get(0).setLineUnmatched(linesToAdd);
        filesToCompare.get(0).setUnMatchingRecord((int) linesToAdd.stream().count());
        filesToCompare.get(0).setMatchingRecord(filesToCompare.get(0).getTotalRecord()-filesToCompare.get(0).getUnMatchingRecord());

       // System.out.println("Lines unmatched file 1\n"+filesToCompare.get(0).getLineUnmatched());
//        linesToAdd.clear();

        for (String name: linesFileTwo.keySet()) {
            String key = name.toString();
            String value= linesFileTwo.get(name).toString();
//			System.out.println("\nkey:"+key + "\nvalue:" + value);

            if(!linesFileOne.containsValue(value)){
                linesToAdd2.add(key);
                //System.out.println("File one don't have value: "+value);
            }
            else if(!linesFileOne.containsKey(key)){
               // System.out.println("File one don't have key: "+key);
                linesToAdd2.add(key);
            }
        }

        filesToCompare.get(1).setLineUnmatched(linesToAdd2);
        filesToCompare.get(1).setUnMatchingRecord((int) linesToAdd2.stream().count());
        filesToCompare.get(1).setMatchingRecord(filesToCompare.get(1).getTotalRecord()-filesToCompare.get(1).getUnMatchingRecord());
       // System.out.println("Lines unmatched file 2\n"+filesToCompare.get(1).getLineUnmatched());
       // System.out.println("Files to compare full: "+filesToCompare.size());
        //filesToCompare.forEach(System.out::println);
        return filesToCompare;
    }
}



