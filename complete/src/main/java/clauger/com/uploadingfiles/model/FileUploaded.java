package clauger.com.uploadingfiles.model;

import java.util.ArrayList;
import java.util.HashMap;

public class FileUploaded {
    private String name;
    private Integer totalRecord;
    private Integer matchingRecord;
    private Integer unMatchingRecord;
    private HashMap<String, String> lines;
    private ArrayList<String> lineUnmatched;

    public FileUploaded() {
    }

    public FileUploaded(String name, Integer totalRecord, Integer matchingRecord, Integer unMatchingRecord, HashMap<String, String> lines, ArrayList<String> lineUnmatched) {
        this.name = name;
        this.totalRecord = totalRecord;
        this.matchingRecord = matchingRecord;
        this.unMatchingRecord = unMatchingRecord;
        this.lines = lines;
        this.lineUnmatched = lineUnmatched;
    }

    @Override
    public String toString() {
        return "FileUploaded{" +
                "name='" + name + '\'' +
                ", totalRecord=" + totalRecord +
                ", matchingRecord=" + matchingRecord +
                ", unMatchingRecord=" + unMatchingRecord +
                ", lines=" + lines +
                ", lineUnmatched=" + lineUnmatched +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Integer totalRecord) {
        this.totalRecord = totalRecord;
    }

    public Integer getMatchingRecord() {
        return matchingRecord;
    }

    public void setMatchingRecord(Integer matchingRecord) {
        this.matchingRecord = matchingRecord;
    }

    public Integer getUnMatchingRecord() {
        return unMatchingRecord;
    }

    public void setUnMatchingRecord(Integer unMatchingRecord) {
        this.unMatchingRecord = unMatchingRecord;
    }

    public HashMap<String, String> getLines() {
        return lines;
    }

    public void setLines(HashMap<String, String> lines) {
        this.lines = lines;
    }

    public ArrayList<String> getLineUnmatched() {
        return lineUnmatched;
    }

    public void setLineUnmatched(ArrayList<String> lineUnmatched) {
        this.lineUnmatched = lineUnmatched;
    }
}