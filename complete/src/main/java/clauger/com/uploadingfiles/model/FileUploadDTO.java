package clauger.com.uploadingfiles.model;

import java.util.ArrayList;
import java.util.HashMap;

public class FileUploadDTO {
    private String name;
    private Integer totalRecord;
    private Integer matchingRecord;
    private Integer unMatchingRecord;
    private ArrayList<LineReport> lineUnmatched;

    public FileUploadDTO(){

    }
    public FileUploadDTO(String name,
                         Integer totalRecord,
                         Integer matchingRecord,
                         Integer unMatchingRecord,
                         ArrayList<LineReport> lineUnmatched) {
        this.name = name;
        this.totalRecord = totalRecord;
        this.matchingRecord = matchingRecord;
        this.unMatchingRecord = unMatchingRecord;
        this.lineUnmatched = lineUnmatched;
    }

    @Override
    public String toString() {
        return "FileUploadDTO{" +
                "name='" + name + '\'' +
                ", totalRecord=" + totalRecord +
                ", matchingRecord=" + matchingRecord +
                ", unMatchingRecord=" + unMatchingRecord +
                ", lineUnmatched=" + lineUnmatched +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Integer totalRecord) {
        this.totalRecord = totalRecord;
    }

    public Integer getMatchingRecord() {
        return matchingRecord;
    }

    public void setMatchingRecord(Integer matchingRecord) {
        this.matchingRecord = matchingRecord;
    }

    public Integer getUnMatchingRecord() {
        return unMatchingRecord;
    }

    public void setUnMatchingRecord(Integer unMatchingRecord) {
        this.unMatchingRecord = unMatchingRecord;
    }

    public ArrayList<LineReport> getLineUnmatched() {
        return lineUnmatched;
    }

    public void setLineUnmatched(ArrayList<LineReport> lineUnmatched) {
        this.lineUnmatched = lineUnmatched;
    }
}
