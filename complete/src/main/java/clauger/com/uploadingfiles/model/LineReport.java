package clauger.com.uploadingfiles.model;

public class LineReport {
    private String transactionDate;
    private String transactionAmount;
    private String transactionNarrative;
    private String transactionDescription;
    private String transactionID;
    private String transactionType;
    private String walletReference;

    public LineReport() {

    }

    @Override
    public String toString() {
        return "LineReport{" +
                "transactionDate='" + transactionDate + '\'' +
                ", transactionAmount='" + transactionAmount + '\'' +
                ", transactionNarrative='" + transactionNarrative + '\'' +
                ", transactionDescription='" + transactionDescription + '\'' +
                ", transactionID='" + transactionID + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", walletReference='" + walletReference + '\'' +
                '}';
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionNarrative() {
        return transactionNarrative;
    }

    public void setTransactionNarrative(String transactionNarrative) {
        this.transactionNarrative = transactionNarrative;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getWalletReference() {
        return walletReference;
    }

    public void setWalletReference(String walletReference) {
        this.walletReference = walletReference;
    }

    public LineReport(String transactionDate, String transactionAmount, String transactionNarrative, String transactionDescription, String transactionID, String transactionType, String walletReference) {
        this.transactionDate = transactionDate;
        this.transactionAmount = transactionAmount;
        this.transactionNarrative = transactionNarrative;
        this.transactionDescription = transactionDescription;
        this.transactionID = transactionID;
        this.transactionType = transactionType;
        this.walletReference = walletReference;
    }
}
