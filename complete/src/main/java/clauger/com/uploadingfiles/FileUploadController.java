package clauger.com.uploadingfiles;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import clauger.com.uploadingfiles.model.FileUploadDTO;
import clauger.com.uploadingfiles.model.FileUploaded;
import clauger.com.uploadingfiles.model.LineReport;
import clauger.com.uploadingfiles.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import clauger.com.uploadingfiles.storage.StorageFileNotFoundException;
import clauger.com.uploadingfiles.business.BusinessInterface;



@Controller
public class FileUploadController {

	private final StorageService storageService;
	private final BusinessInterface businessInterface;

	@Autowired
	public FileUploadController(StorageService storageService, BusinessInterface businessInterface) {
		this.storageService = storageService;
		this.businessInterface = businessInterface;
	}



	@GetMapping("/")
	public String listUploadedFiles(Model model) throws IOException {

		model.addAttribute("files", storageService.loadAll().map(
				path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
						"serveFile", path.getFileName().toString()).build().toUri().toString())
				.collect(Collectors.toList()));

		return "uploadForm";
	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}

	@PostMapping("/")
	public String handleFileUpload(@RequestParam("file") MultipartFile file,
			RedirectAttributes redirectAttributes) {

		storageService.store(file);
		redirectAttributes.addFlashAttribute("message",
				"You successfully uploaded " + file.getOriginalFilename() + "!");

		return "redirect:/";
	}
	@RequestMapping("/about")
	public String compare(){
		return "about";
	}

	@RequestMapping("/restart")
	public String restart( RedirectAttributes redirectAttributes) throws IOException {
		String message = "";
		message = businessInterface.deleteAll();
		redirectAttributes.addFlashAttribute("message",	message+ " !");
		return "redirect:/";
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}
	@PostMapping("/upload")

	public String uploadFiles(@RequestParam("files") MultipartFile[] files,
							  RedirectAttributes redirectAttributes,
							  Model model) {

		HashMap<String,ArrayList<FileUploaded> > result = businessInterface.uploadFilesBusiness(files);
		String message = "";

		for (Map.Entry<String,ArrayList<FileUploaded> >pair : result.entrySet()) {

			if(!pair.getValue().isEmpty()){

				redirectAttributes.addFlashAttribute("message",	pair.getKey() + " !");
				//redirectAttributes.addFlashAttribute("FilesUploaded",pair.getValue());
				//System.out.println("After Return: "+businessInterface.filesToDto(pair.getValue()));
				redirectAttributes.addFlashAttribute("FilesUploaded",businessInterface.filesToDto(pair.getValue()));
			}
			else{
				redirectAttributes.addFlashAttribute("message",	pair.getKey() + " !");
			}
		}
		return "redirect:/";
	}

}
