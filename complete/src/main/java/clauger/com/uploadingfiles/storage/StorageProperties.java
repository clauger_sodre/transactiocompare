package clauger.com.uploadingfiles.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {


	private String location = "/";
	public String getLocation() {
		String oS =System.getProperty("os.name");
		if(oS.equals("Linux")){

			return location;
		}
		else {
			location= "C:/temp";
			return location;
		}
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
